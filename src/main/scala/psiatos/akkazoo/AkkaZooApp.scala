package psiatos.akkazoo

import akka.actor.ActorSystem
import psiatos.akkazoo.actor.ZKNodeActor

object AkkaZooApp extends App {

  val system = ActorSystem("AkkaZoo")
  val superVisor = system.actorOf(ZKNodeActor.props, "zkNode")
}
