package psiatos.akkazoo.actor

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.kafka.ProducerSettings
import akka.kafka.ProducerMessage
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import psiatos.akkazoo.JsonSupport

import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
  * Actor responsible for calling external QNRG REST API and pushing response to Kafka.
  *
  * @see [[https://qrng.anu.edu.au QRNG]]
  * @author Petros Siatos
  */
class WorkerActor extends Actor with ActorLogging with Directives with JsonSupport {

  implicit val system = context.system
  implicit val executionContext = context.system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val timeout = Timeout(5 seconds)

  import WorkerActor._

  val config = system.settings.config.getConfig("akka.kafka.producer")
  val producerSettings =
    ProducerSettings(config, new StringSerializer, new StringSerializer)
      .withBootstrapServers(KAFKA_SERVER)
  val kafkaProducer = producerSettings.createKafkaProducer()

  override def receive: Receive = {
    case GetRandomNumber =>
      log.info("Requesting Random Number")
      Http(context.system).singleRequest(HttpRequest(
        uri = Uri(API_ENDPOINT).withQuery(
          Query(PARAM_DATA_TYPE -> DataType.uint16.toString.toLowerCase(), PARAM_LENGTH -> "1"))))
        .flatMap(r => Unmarshal(r.entity).to[NumberResponse])
        .map(_.data(0))
        .onComplete {
          case Success(number) => {
            log.info("Number {}", number)
            kafkaProducer.send(new ProducerRecord(KAFKA_TOPIC, "null", number.toString))
          }
          case Failure(err) => {
            // TODO: Handle Failure as needed.
            log.error("Boomz {}", err.getMessage)
            err.printStackTrace()
          }
        }

    case _@value => {
      log.debug("Incoming unknown message {}", value)
    }

  }

}

object WorkerActor {

  import DataType._

  val API_ENDPOINT = "https://qrng.anu.edu.au/API/jsonI.php"
  val PARAM_LENGTH = "length"
  val PARAM_DATA_TYPE = "type"
  val PARAM_HEX_SIZE = "size"
  val KAFKA_SERVER = "localhost:9092"
  val KAFKA_TOPIC = "numbers"

  def props = Props[WorkerActor]

  // Messages of WorkerActor
  sealed trait WorkerMessage

  final object GetRandomNumber extends WorkerMessage

  // Response DTOs
  final case class NumberResponse(`type`: DataType, length: Int, data: Array[Long], success: Boolean)

}

object DataType extends Enumeration {
  type DataType = Value
  val uint8, uint16, hex16 = Value
}
