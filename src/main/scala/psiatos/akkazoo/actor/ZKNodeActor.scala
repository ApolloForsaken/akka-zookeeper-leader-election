package psiatos.akkazoo.actor

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{Actor, ActorLogging, OneForOneStrategy, Props}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import org.reactivestreams.Publisher
import uk.co.appministry.akka.zk.ZkResponseProtocol._
import uk.co.appministry.akka.zk._

import scala.concurrent.duration._

/**
  * Zookeeper Client using appministri/akka-zk library.
  *
  * @see [[https://index.scala-lang.org/appministry/akka-zk/akka-zk/0.1.0?target=_2.12 appministroy/akka-zk]]
  * @author Petros Siatos
  */
class ZKNodeActor extends Actor with ActorLogging {

  import ZKNodeActor._
  import context._

  implicit val system = context.system
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  lazy val workerActor = context.actorOf(WorkerActor.props, "worker")
  // TODO: use preStart to init children
  val zkClient = context.actorOf(Props(new ZkClientActor), "zkClient")
  context.watch(zkClient)
  var electionNum: Long = 9999999999l
  var reactiveStreamsPublisher: Publisher[ZkClientStreamProtocol.StreamResponse] = null

  override def preStart(): Unit = {
    log.info("Starting")
    resetElectionNum()
    self ! Connect
  }

  private def resetElectionNum() = electionNum = 9999999999l

  override def supervisorStrategy = OneForOneStrategy() {
    case _: ZkClientConnectionFailedException =>
      // ZooKeeper client was unable to connect to the server for `connectionAttempts` times.
      // The client is stopped and a new actor has to be created.
      disconnect()
      Restart
  }

  /**
    * Client starts as disconnected from ZK.
    *
    * @return
    */
  override def receive: Receive = disconnected

  /**
    * Client is disconnected from ZK. Can only try to reconnect.
    */
  def disconnected: Receive = {
    case KeepAlive => // Ignoring since we are disconnected.
    case Connect => connectToZk()

    case Connected(request, _reactiveStreamsPublisher) => {
      log.info("Connected to ZooKeeper")
      reactiveStreamsPublisher = _reactiveStreamsPublisher
      become(connected)
      self ! CreateElectionNode
      self ! KeepAlive
    }

    case _@ msg => log.debug("I don't talk to strangers {}", msg)
  }

  /**
    * Client is connected to ZK. Ready to be elected as Leader if needed.
    */
  def connected: Receive = {
    case CreateElectionNode =>
      log.info("Creating Election Node")
      zkClient ! ZkRequestProtocol.CreateEphemeral(path = ZNODE_ELECTION_CHILD_PREFIX, sequential = true, createParents = true)

    case Created(_, _electionNum) =>
      electionNum = _electionNum.replace(ZNODE_ELECTION_CHILD_PREFIX, "").toLong
      log.info("I am {}", electionNum)
      self ! CheckLeadership

    case CheckLeadership =>
      log.info("Checking leadership")
      zkClient ! ZkRequestProtocol.GetChildren(ZNODE_ELECTION)

    case Children(_, nodes) =>
      val nodeNums = nodes.map(_.replace(CHILD_PREFIX, "").toLong)
      log.info("Nodes {}", nodeNums)
      log.info("I am {}", electionNum.toLong)

      val previousLeader: Option[String] = nodeNums
        .sortWith(_ > _)
        .filter(c => c < electionNum.toLong)
        .headOption
        .map(padElectionNum)

      previousLeader match {
        case None =>
          become(leader)
          self ! InitLeader
        case Some(previousL) => watchPreviousNode(previousL)
      }

    case KeepAlive => keepAlive()
    case Connected(_, _) => disconnect()
    case AwaitingConnection => disconnect()
    case Disconnected => disconnect()

    case _@ msg => log.debug("Incoming unknown message {}", msg)
  }

  /**
    * Client is connected to ZK and currently elected Leader. Does the hard work (just delegates actually)
    */
  def leader: Receive = {
    case InitLeader =>
      log.info("I am the leader! {}", electionNum)
      context.system.scheduler.scheduleOnce(delay = LEADER_KEEP_ALIVE_DELAY, self, KeepAlive)
      context.system.scheduler.scheduleOnce(delay = WORKER_DELAY, self, Work)

    case Work =>
      log.info("Leader gets the work done!")
      workerActor ! WorkerActor.GetRandomNumber
      context.system.scheduler.scheduleOnce(delay = LEADER_KEEP_ALIVE_DELAY, self, KeepAlive)
      context.system.scheduler.scheduleOnce(delay = WORKER_DELAY, self, Work)

    case KeepAlive =>
      log.info("Keep Alive")
      connectToZk()

    case Connected(_, _) => disconnect()
    case AwaitingConnection => disconnect()
    case Disconnected => disconnect()

    case _@ msg => log.debug("Don't bother the leader {}", msg)
  }

  private def connectToZk() = {
    zkClient ! ZkRequestProtocol.Connect(connectionString = "0.0.0.0:2181",
      connectionAttempts = 1,
      sessionTimeout = 500 seconds)
  }

  /**
    * Asking to connect to ZK (through [[ZkClientActor]] to check if connection is still alive.
    * if client is not connected it will send a [[ZkResponseProtocol.AwaitingConnection]]
    */
  private def keepAlive() = {
    log.info("Keep Alive")
    context.system.scheduler.scheduleOnce(delay = KEEP_ALIVE_DELAY, self, KeepAlive)
    connectToZk()
  }

  /**
    * Starts death watch on the previous node to be leader, and if it fails checks again if this one is leader
    * or otherwise starts a new death watch on the next alive previous node.
    * @param previousNode the previous node election number.
    */
  private def watchPreviousNode(previousNode: String): Unit = {
    val previous_znode = ZNODE_ELECTION_CHILD_PREFIX + previousNode
    log.info("Subscribing to {}", previous_znode)

    zkClient ! ZkRequestProtocol.SubscribeDataChanges(ZNODE_ELECTION_CHILD_PREFIX + previousNode)

    Source.fromPublisher[ZkClientStreamProtocol.StreamResponse](reactiveStreamsPublisher).filter(message =>
      message match {
        case m: ZkClientStreamProtocol.DataChange => m.event.underlying.getPath.equals(previous_znode)
        case _ => true
      }
    ).runWith(Sink.foreach(_ => checkLeadership(previousNode)))
  }

  /**
    * Sends message to self to restart the process of checking if it's leader
    * or otherwise start new death watch on the next alive previous node.
    * @param previousNode the previous node election number.
    */
  private def checkLeadership(previousNode: String) = {
    log.info("Node {} is dead.", previousNode)
    self ! CheckLeadership
  }

  /**
    * Changes state to disconnected and restarts election procedure (to receive new election number)
    * This is called in case connection with Zookeeper is lost.
    */
  private def disconnect() = {
    log.info("Disconnecting.")
    become(disconnected)
    resetElectionNum()
    self ! Connect
  }

}

object ZKNodeActor {

  val ZNODE_ELECTION = "/ELECTION"
  val CHILD_PREFIX = "n_"
  val ZNODE_ELECTION_CHILD_PREFIX = ZNODE_ELECTION + "/" + CHILD_PREFIX
  val KEEP_ALIVE_DELAY = 3 seconds
  val WORKER_DELAY = 10 seconds
  val LEADER_KEEP_ALIVE_DELAY = WORKER_DELAY - Duration(1, "seconds")

  def props = Props[ZKNodeActor]

  /**
    * Pad the election number with leading zeros to have a length of 10 characters (i.e. 13 -> "0000000013").
    *
    * @see [[https://stackoverflow.com/questions/8131291/how-to-convert-an-int-to-a-string-of-a-given-length-with-leading-zeros-to-align Stack Overflow]]
    * @param electionNum the number to be padded.
    * @return the result string.
    */
  def padElectionNum(electionNum: Long) = f"${electionNum}%010d"

  // Messages of ZKNodeActor
  sealed trait ZKNodeMessage

  final object Connect extends ZKNodeMessage

  final object KeepAlive extends ZKNodeMessage

  final object CreateElectionNode extends ZKNodeMessage

  final object CheckLeadership extends ZKNodeMessage

  final object InitLeader extends ZKNodeMessage

  final object Work extends ZKNodeMessage

}
