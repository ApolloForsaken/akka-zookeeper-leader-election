package psiatos.akkazoo

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import psiatos.akkazoo.actor.WorkerActor.NumberResponse
import psiatos.akkazoo.actor.DataType
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, RootJsonFormat}

/**
  * Support for JSON unmarshalling.
  */
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  import spray.json._

  implicit val dataTypeFormat = new EnumJsonConverter(DataType)
  implicit val numberResponseFormat = jsonFormat4(NumberResponse)
}

/**
  * @see [[https://github.com/spray/spray-json/issues/200 GitHub - Spray]]
  *
  * Based on the code found: [[https://groups.google.com/forum/#!topic/spray-user/RkIwRIXzDDc Google-Groups - Spray]]
  */
class EnumJsonConverter[T <: scala.Enumeration](enu: T) extends RootJsonFormat[T#Value] {
  override def write(obj: T#Value): JsValue = JsString(obj.toString)

  override def read(json: JsValue): T#Value = {
    json match {
      case JsString(txt) => enu.withName(txt)
      case somethingElse => throw DeserializationException(s"Expected a value from enum $enu instead of $somethingElse")
    }
  }
}