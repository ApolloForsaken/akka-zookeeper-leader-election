# Akka - Zookeeper Leader Election
Akka application using zookeeper to elect cluster leader.

## Basic Info
This is a simple Akka App using zookeeper client library [appministry / akka-zk](https://index.scala-lang.org/appministry/akka-zk/akka-zk/0.1.0?target=_2.12)
to connect and elect a leader.

Further reading on  leader election.
[Zookeeper - Leader Election](https://zookeeper.apache.org/doc/r3.4.12/recipes.html)

Draw.io diagram included at `src/main/resources/architecture-draw-io.xml`

### Connected (Waiting to be elected Leader)
When an instance connects and is not a leader, it watches  instance-1's  state.
If it fails, it will check again if it has become the leader otherwise it will start watching again instance-1 that is still alive.

### Leader
The instance which is elected as leader will get random numbers from
an external API [QRNG API](https://qrng.anu.edu.au)
and sending them to kafka topic `numbers`

### Additional Info
When an instance disconnects it will return to the disconnected state (regardless if it was leader) and the process for leader election will restart.
If Zookeeper fails, all instances will disconnect, thus there will be no leader till Zookeeper is back online and at least one instance can reconnect.

## Getting Started
- Start Apache Zookeeper and Kafka
As shown here [Apache Kafka Quickstart #Start the server](https://kafka.apache.org/quickstart#quickstart_startserver)

- Listen to Kafka Topic `numbers`
As shown here [Apache Kafka Quickstart #Start a consumer](https://kafka.apache.org/quickstart#quickstart_consume)
Run from your Kafka folder:
> bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic numbers --from-beginning

- Run multiple instances of this app
> sbt run

- Stop and start as you like

- Enjoy
