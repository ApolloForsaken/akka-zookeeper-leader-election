name := "akka-zookeeper-leader-election"

version := "0.1"

scalaVersion := "2.12.6"
val akkaVersion = "2.5.13"
val akkaHttpVersion = "10.1.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor_2.12" % akkaVersion withSources() withJavadoc(),
  "com.typesafe.akka" % "akka-testkit_2.12" % akkaVersion % Test withSources() withJavadoc(),
  "com.typesafe.akka" % "akka-stream_2.12" % akkaVersion withSources() withJavadoc(),
  "com.typesafe.akka" % "akka-http_2.12" % akkaHttpVersion withSources() withJavadoc(),
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion withSources() withJavadoc(),
  "com.typesafe.akka" % "akka-slf4j_2.12" % akkaVersion withSources() withJavadoc(),
  "com.typesafe.akka" %% "akka-stream-kafka" % "0.22",
  "uk.co.appministry" %% "akka-zk" % "0.1.0",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"

  // could try for kafka: https://github.com/cakesolutions/scala-kafka-client#akka-integration
  // "net.cakesolutions" %% "scala-kafka-client-akka" % "1.1.0",
)
